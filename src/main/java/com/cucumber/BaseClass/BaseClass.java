package com.cucumber.BaseClass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BaseClass {
    public static WebDriver driver;
	public static void browseropen(String browseropen) {

		try {
			if (browseropen.equalsIgnoreCase("chrome")) {
				
				System.setProperty("webdriver.chrome.driver", "C:\\Users\\Acer\\eclipse-workspace\\HelloWorld\\src\\BrowserDriver\\chromedriver.exe");
			    driver = new ChromeDriver ();
			}else if (browseropen.equalsIgnoreCase("ie")) {
				
				System.setProperty("webdriver.ie.driver", "C:\\Users\\Acer\\eclipse-workspace\\HelloWorld\\src\\BrowserDriver\\InternetExploredrier.exe");
				driver = new InternetExplorerDriver ();
				
			} else if (browseropen.equalsIgnoreCase("firefox")) {
				
				System.setProperty("webdriver.gecko.driver", "C:\\Users\\Acer\\eclipse-workspace\\HelloWorld\\src\\BrowserDriver\\geckodriver.exe");
				driver = new FirefoxDriver ();
				
			}else {
				System.out.println("Invalid browsername");
			}
			driver.manage().window().maximize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		}  
	
	      public static void getUrl(String Url) {
          try {
			driver.get(Url);
		} catch (Exception e) {
			e.printStackTrace();
		}
          
	}
	      public static void driverClose() {
          
	    	  try {
				driver.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	         public static void inputText(WebElement element, String value) {
             try {
				element.sendKeys(value);
			} catch (Exception e) {
				e.printStackTrace();
			}
             
			}
		



}
